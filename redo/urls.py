from django.urls import path
from redo import views

urlpatterns = [
    path('', views.library, name='library'),
    path('get_json/', views.get_json, name='get_json'),
]