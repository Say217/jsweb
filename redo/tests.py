from django.test import TestCase, Client
from django.urls import resolve
from .views import library, get_json

class RedoTest(TestCase):
    def test_tdd_library_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_tdd_using_library_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'library.html')

    def test_tdd_using_library_func(self):
        found = resolve('')
        self.assertEqual(found.func, index)