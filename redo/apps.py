from django.apps import AppConfig


class RedoConfig(AppConfig):
    name = 'redo'
