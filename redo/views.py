from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


# Create your views here.

def library(request):
    return render(request, 'library.html')


def get_json(request):
	json_url = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
	json = json_url.json()
	return JsonResponse(json)