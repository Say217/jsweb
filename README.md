## Status pipelines:
[![pipeline status](https://gitlab.com/Say217/django-web/badges/master/pipeline.svg)](https://gitlab.com/Say217/django-web/commits/master)

## Status code coverage:
[![coverage report](https://gitlab.com/Say217/django-web/badges/master/coverage.svg)](https://gitlab.com/Say217/django-web/commits/master)

## Link heroku:
https://sayid-django.herokuapp.com/